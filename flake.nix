{
  description = "A minimal Jekyll flake.";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils = { url = "github:numtide/flake-utils"; };
  };

  outputs = { self, flake-compat, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        ruby = pkgs.ruby;
        gems = pkgs.bundlerEnv {
          inherit ruby;
          name = "pages-env";
          gemdir = self;
        };
      in with pkgs; {
        packages = rec {
          site = stdenv.mkDerivation rec {
            src = ./.;
            name = "stranger.systems";
            buildInputs = [ gems bundler ruby ];
            buildPhase = ''
              jekyll build
            '';
            installPhase = ''
              mkdir -p $out
              cp -r _site/* $out
            '';
          };
          default = site;
        };
        devShell =
          mkShell { buildInputs = [ bundix bundler gems ruby zlib xz ]; };
      });
}
